package tp.rest;

import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;

public class MyClient {
    private Service service;
    private JAXBContext jc;

    private static final QName qname = new QName("", "");
    
    private static final String url = "http://127.0.0.1:8084";

    public MyClient() {
        try { 
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
        } catch (JAXBException je) {
            System.out.println("Cannot create JAXBContext " + je);
        }
    }

    public void add_animal(Animal animal) throws JAXBException {
    	System.out.println("-- Ajout d'un animal : "+animal.toString());
        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "POST");
        Source result = dispatcher.invoke(new JAXBSource(jc, animal));
        printSource(result);
    }
    /*
     * appel au service qui permet de  Supprimer l'ensmble des animaux
     */
    public void delete_animals() throws JAXBException {
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "DELETE");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        printSource(result);
    }
    /*
     * vider tous les cages et ajouter l animal dans ça cages 
     * 
     */
    public void update_animals(Animal animal) throws JAXBException {
    	 System.out.println("-- Modifier des animaux : "+animal.toString() +"--");
    	 
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "PUT");
        Source result = dispatcher.invoke(new JAXBSource(jc, animal));
        printSource(result);
    	
    }
    /**
     * ajouter un animal identifié par animl id
     * 
     */
    public void add_animal_By_Id(Animal animal, UUID animal_id) {
    	System.out.println("-- Ajout d'un animal avec son id="+animal_id+" : "+animal.toString());
       
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/" + animal_id);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "POST");
        try {
			Source result = dispatcher.invoke(new JAXBSource(jc, animal));
			printSource(result);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
   
/****
 * Modifier l animal identifié par animal_id
 * @throws JAXBException 
 * 
 */
    public void update_animal_By_Id(UUID animal_id, Animal animal) throws JAXBException {
    	System.out.println("-- Modification d'un animal avec son id="+animal_id+" : "+animal.toString());
    	
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/"+ animal_id);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "PUT");
  
        Source result = dispatcher.invoke(new JAXBSource(jc,animal));
        printSource(result);	
    }
    /**
     * Supprimer un animal identifié par animal_id
     * @param animal_id
     * @throws JAXBException
     */
    public void delete_animal_By_Id(UUID animal_id) throws JAXBException {
    	System.out.println("-- Suppression d'un animal avec son id="+animal_id.toString());
    	
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals/"+ animal_id);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "DELETE");
        Source result = dispatcher.invoke(new JAXBSource(jc, new Animal()));
        printSource(result);	
    }
    /**
     * Afficher tous les animaux
     *
     */
    
    public void afficher_animamls() {
    	System.out.println("-- Afficher les animaux--");
    	
    	service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/animals");
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
        Source result = dispatcher.invoke(null);
        printSource(result);	
        System.out.println("");
    }
    /**Recherche par nom 
     * @throws JAXBException
     */
    public void findByName(String name) throws JAXBException {
        System.out.println("-- Recherche d'un animal par son nom="+name);

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/find/byName/" + name);
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
	    Source result = dispatcher.invoke(null);
	    printSource(result);
    }
    /**
     * Rechercher un animal à partir de sa position
     * @throws JAXBException
     * @throws MalformedURLException 
     * @throws UnsupportedEncodingException 
     */
    public void findAt(Position position) throws JAXBException, UnsupportedEncodingException {
        System.out.println("-- Recherche d'un animal  la position-- ="+position.toString());

        service = Service.create(qname);
        service.addPort(qname, HTTPBinding.HTTP_BINDING, url + "/find/at/" + position.toString());
        Dispatch<Source> dispatcher = service.createDispatch(qname, Source.class, Service.Mode.MESSAGE);
        Map<String, Object> requestContext = dispatcher.getRequestContext();
        requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "GET");
	    Source result = dispatcher.invoke(null);
	    printSource(result);
	   
    }


    public void printSource(Source s) {
        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            transformer.transform(s, new StreamResult(System.out));
            System.out.println("");
        
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String args[]) throws Exception {
        MyClient client = new MyClient();
        client.afficher_animamls();
        
        client.delete_animals();
        client.afficher_animamls();
        client.add_animal(new Animal("bibi", "Rouen", "Panda", UUID.randomUUID()));
        client.add_animal(new Animal("hocco", "Paris", "Hocco unicorne", UUID.randomUUID()));
        client.afficher_animamls();
        client.update_animals(new Animal("Lagotriche","Rouen","Lagotriche  queue jaune", UUID.randomUUID()));
        client.afficher_animamls();
        client.add_animal(new Animal("Océanite", "Somalie", "Océanite de Matsudaira", UUID.randomUUID()));
        Animal ara_de_spix = new Animal("Ara de Spix", "Rouen", "Ara de Spix", UUID.randomUUID());
		client.add_animal(ara_de_spix);
		Animal galado_de_rondo = new Animal("Galago", "Bihorel", "Galago de Rondo", UUID.randomUUID());
		client.add_animal(galado_de_rondo);
		client.add_animal(new Animal("Palette des Sulu", "Londres", "Palette des Sulu", UUID.randomUUID()));
		client.add_animal(new Animal("Kouprey", "Paris", "Kouprey", UUID.randomUUID()));
		client.add_animal(new Animal("Tuit-tuit", "Paris", "Tuit-tuit", UUID.randomUUID()));
		Animal saiga = new Animal("saga", "Canada", "Saga", UUID.randomUUID());
		client.add_animal(saiga);
		client.add_animal(new Animal("Gazelle","Brésil","Gazelle de Mhorr",UUID.randomUUID()));
		client.add_animal(new Animal("Inca", "Porto-Vecchio", "Inca de Bonaparte", UUID.randomUUID()));
		client.afficher_animamls();
		client.add_animal(new Animal("Rhinocros ", "Villers-Bocage", "Rhinocros de Java", UUID.randomUUID()));
		client.add_animal(new Animal("Rale", "Montreux", "Rale de Zapata", UUID.randomUUID()));
		for(int i=0; i<101; i++) {
			client.add_animal(new Animal("dalmatiens", "usa", "Dalmatiens", UUID.randomUUID()));
		}
        client.afficher_animamls();
        client.findByName(galado_de_rondo.getName());
        client.findAt(new Position(49.443889d, 1.103333d));
        
        
      
    }
}
