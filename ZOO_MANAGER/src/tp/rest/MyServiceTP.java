package tp.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshallerHandler;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.Source;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.http.HTTPBinding;
import javax.xml.ws.http.HTTPException;

import org.omg.CosNaming.NamingContextPackage.NotFound;

import tp.model.Animal;
import tp.model.AnimalNotFoundException;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

@WebServiceProvider
@ServiceMode(value = Service.Mode.MESSAGE)
public class MyServiceTP implements Provider<Source> {

    public final static String url = "http://127.0.0.1:8084/";

    public static void main(String args[]) {
        Endpoint e = Endpoint.create(HTTPBinding.HTTP_BINDING, new MyServiceTP());

        e.publish(url);
        System.out.println("Service started, listening on " + url);
        // pour arrêter : e.stop();
    }

    private JAXBContext jc;

    @javax.annotation.Resource(type = Object.class)
    protected WebServiceContext wsContext;

    private Center center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");

    public MyServiceTP() {
        try {
            jc = JAXBContext.newInstance(Center.class, Cage.class, Animal.class, Position.class);
        } catch (JAXBException je) {
            System.out.println("Exception " + je);
            throw new WebServiceException("Cannot create JAXBContext", je);
        }

        // Fill our center with some animals
        Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<>(Arrays.asList(
                        new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID()),
                        new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID())
                ))
        );

        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<>(Arrays.asList(
                        new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
                ))
        );
        Cage rouen = new Cage("Rouen", new Position(49.443889d, 1.103333d), 50, 
        		new LinkedList<Animal>());
        Cage paris = new Cage("Paris", new Position(48.856578, 2.351828d), 50, new LinkedList<Animal>());
        Cage somalie = new Cage("Somalie", new Position(2.333333d, 48.85d), 50, new LinkedList<Animal>());
        Cage bihorel = new Cage("Bihorel", new Position(49.455278d, 1.116944d), 50, new LinkedList<Animal>());
        Cage londre = new Cage("Londres", new Position(51.504872d, -0.07857d), 50, new LinkedList<Animal>());
        Cage canada = new Cage("Canada", new Position(43.2d, -80.38333d), 50, new LinkedList<Animal>());
        Cage bresil = new Cage("Brésil", new Position(-53.4715856d, -19.4435386),50, new LinkedList<Animal>());
        Cage porto_vecchio = new Cage("Porto-Vecchio", new Position(41.5895241d, 9.2627d), 50,new LinkedList<Animal>());
        Cage montreux = new Cage("Montreux", new Position(46.4307133d,6.9113575d), 50, new LinkedList<Animal>());
        Cage villers_bocage = new Cage("Villers-Bocage", new Position(50.0218d,2.3261d),50, new LinkedList<Animal>());
        
        center.getCages().addAll(Arrays.asList(usa, amazon, rouen, paris, somalie,bihorel,londre, canada, bresil, porto_vecchio, montreux, villers_bocage));
        
    }

    public Source invoke(Source source) {
        MessageContext mc = wsContext.getMessageContext();
        String path = (String) mc.get(MessageContext.PATH_INFO);
        String method = (String) mc.get(MessageContext.HTTP_REQUEST_METHOD);

        // determine the targeted ressource of the call
        try {
            // no target, throw a 404 exception.
            if (path == null) {
                throw new HTTPException(404);
            }
            // "/animals" target - Redirect to the method in charge of managing this sort of call.
            else if (path.startsWith("animals")) {
                String[] path_parts = path.split("/");
                switch (path_parts.length){
                    case 1 :
                        return this.animalsCrud(method, source);
                    case 2 :
                        return this.animalCrud(method, source, path_parts[1]);
                    default:
                        throw new HTTPException(404);
                }
            }
            else if (path.startsWith("find/")) {
            	String[] path_parts = path.split("/");
              
                if( path_parts.length != 3) throw new HTTPException(404);
                switch(path_parts[1]) {
		            case "byName": 
		            	//Recherche d'un animal par son nom
		            	return this.findByName(method, source, path_parts[2]);
		            case "at":
		            	return this.findAt(method, source, path_parts[2]);
		            default:
		            	throw new HTTPException(503);
                }
            }
            else if ("coffee".equals(path)) {
                throw new HTTPException(418);
            }
            else {
                throw new HTTPException(404);
            }
        } catch (JAXBException e) {
            throw new HTTPException(500);
        }
    }

    /**
     * Method bound to calls on /animals/{something}
     * 
     */
    private Source animalCrud(String method, Source source, String animal_id) throws JAXBException {
        if("GET".equals(method)){
            try {
                return new JAXBSource(this.jc, center.findAnimalById(UUID.fromString(animal_id)));
            } catch (AnimalNotFoundException e) {
                throw new HTTPException(404);
            }
        }
        /*
         * Créer l animal identifié par animal_id
         */
        else if("POST".equals(method)) {
        	Animal animal = unmarshalAnimal(source);
        	this.center.getCages()
        				.stream()
        				.filter(cage -> cage.getName().equals(animal.getCage()))
        				.findFirst()
        				.orElseThrow(() -> new HTTPException(404))
        				.getResidents()
        				.add(animal);
        	return new JAXBSource(jc, center);
  	
        	
        }
        /*
         * Modifie l’animal identifié par { animal_id }
         */
        else if("PUT".equals(method)) {
        	try {
        		Animal s = unmarshalAnimal(source);
        		center.findAnimalById(UUID.fromString(animal_id)).setName(s.getName());
        		center.findAnimalById(UUID.fromString(animal_id)).setSpecies(s.getSpecies());
        		center.findAnimalById(UUID.fromString(animal_id)).setCage(s.getCage());
        	
        		return new JAXBSource(this.jc, center);
            } catch (AnimalNotFoundException e) {
                throw new HTTPException(404);
            }
        }
        /*
         * Supprimer l animal identifié par animal_id
         */
        else if("DELETE".equals(method)){
        	try {
				Animal animal = center.findAnimalById(UUID.fromString(animal_id));
					center.getCages()
					.stream()
					.filter(cage -> cage.getName().equals(animal.getCage()))
					.findFirst()
					 .orElseThrow(() -> new HTTPException(404))
					 .getResidents()
					 .remove(animal);
					return new JAXBSource(this.jc, center);				
			} 
        	catch (AnimalNotFoundException e) {
        		throw new HTTPException(404);
			}
        	
        }
        else{
            throw new HTTPException(405);
        }
    }

    /**
     * Method bound to calls on /animals
     */
    private Source animalsCrud(String method, Source source) throws JAXBException {
        if("GET".equals(method)){
            return new JAXBSource(this.jc, this.center);
        }
        else if("POST".equals(method)){
            Animal animal = unmarshalAnimal(source);
            this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .add(animal);
            return new JAXBSource(this.jc, this.center);
        }
        /*
         * Supprime l'ensemble des animaux du centre 
         * 
         */
        else if("DELETE".equals(method)) {
        	for(Cage cage : center.getCages()) {
        		cage.getResidents().clear();
        	}
        	return new JAXBSource(this.jc, this.center);  	
        }
        /*
         * Modifie l'ensemble des animaux
         * 
         */
        else if ("PUT".equals(method)) {
        
        	Animal animal = unmarshalAnimal(source);
        	 for (Cage cage : (this.center.getCages())) {
        		 for(Animal a : cage.getResidents()) {
        		 a.setName(animal.getName());
        		 a.setSpecies(animal.getSpecies());
        		 }	 
        	 }
        				
        	 return new JAXBSource(this.jc, this.center);   	
        }
        else{
            throw new HTTPException(405);
        }
    }
    /**
     * Recherche d'un animal par son nom  /find/byName/{name}
     */
    private Source findByName(String method, Source source, String name) throws JAXBException {
    	if("GET".equals(method)){
    		//recherche dans tous les cages 
    		for( Cage c : this.center.getCages() ) {
    			for( Animal a : c.getResidents() ) {
    				//retourne l'animal avec le meme nom
    				if( a.getName().equals(name) ) {
    		    		return new JAXBSource(this.jc, a);
    				}
    			}
    		}
    		throw new HTTPException(404);
        }
        else{
            throw new HTTPException(405);
        }
	}
    /**
     * Rechercher un animal par sa position  /find/at/{position}
     */
    private Source findAt(String method, Source source, String position) throws JAXBException {
    	if("GET".equals(method)){
    		//Rechercher dans tous les cages qu ont la meme position que position
    		for( Cage c : this.center.getCages() ) {
    			if( c.getPosition().toString().equals(position) && !c.getResidents().isEmpty()) {
    				//Retourner le prmier animal qui a la meme position
        		    return new JAXBSource(this.jc, c.getResidents().toArray()[0]);
    			}
    		}
    		
    		throw new HTTPException(404);
        }
        else{
     
        	throw new HTTPException(405);
        }
	}
    

    


    private Animal unmarshalAnimal(Source source) throws JAXBException {
        return (Animal) this.jc.createUnmarshaller().unmarshal(source);
    }
   
}
